FROM python:3.9.4-slim-buster
MAINTAINER Neilson Eney <neilsone@yo-yodyne.com>

# Install curl and stuff
RUN apt-get update && apt-get install -y \
    curl \
    gettext-base && \
    rm -rf /var/lib/apt/lists/*

# Upgrade pip & install Ansible
 RUN python -m pip install --upgrade pip && \
     pip install ansible molecule docker

CMD ["/bin/bash"]
